# for irssi >= 0.8.16 (C) Teemu Poikkijoki <poidokoff@gmail.com>
#
# This script is licensed under the NCOLicense, which gives you the following rights:
# - Use this script however you wish
# - Buy the author a beer if you like

use strict;
use Irssi;
use Irssi::Irc;
use vars qw($VERSION %IRSSI);
$VERSION = '0.8';
%IRSSI = (
  authors => "Teemu Poikkijoki", 
  contact => "poidokoff\@gmail.com", 
  name => "replace slack bot", 
  description => "Replaces a slack-bot nick and does some formatting. Oh, and you can ignore images too!", 
  license => "NCOLicense (beer)", 
  url => "https://bitbucket.org/ncolabs/irssi.git", 
  changed => "2017-11-11"
);


my %bot_nicks;
my $ignore_images;

# Reload bot names from irssi settings
sub reload_settings {
  my $nicks = Irssi::settings_get_str('slack_bots');
  $ignore_images = Irssi::settings_get_int('ignore_images');

  %bot_nicks =();
  map { $bot_nicks{$_}=1 } split(/,/, $nicks);
}


# Save bot names to irssi settings
sub save_settings {
  my $settings = join(",", keys %bot_nicks); 

  Irssi::settings_set_str('slack_bots', $settings);
  Irssi::settings_set_int('ignore_images', $ignore_images);
  Irssi::signal_emit("setup changed");
}


# Add bot (/slackwiener add <bot>) 
sub add_bot {
  my $bot = shift;

  $bot =~ s/[^A-Z-a-z0-9_\-]//g;
  $bot_nicks{$bot} = 1;

  Irssi::active_win->print("Added $bot to the list of Slack bots");
  save_settings();
}


# Remove bot (/slackwiener del <bot>)
sub del_bot {
  my $n = shift;

  if(exists($bot_nicks{$n})) {
    delete $bot_nicks{$n};
  }

  Irssi::active_win->print("Deleted $n from the list of Slack bots");
  save_settings();
}

# List bots
sub list_bots {

  Irssi::active_win->print("");
  Irssi::active_win->print("Slack bots registered in Slackwiener");

  foreach my $nick (keys %bot_nicks) {
    Irssi::active_win->print(" -  $nick");
  }

  Irssi::active_win->print("");
}

# Print help
sub print_help {
  Irssi::active_win->print("");
  Irssi::active_win->print("Slackwiener usage: ");
  Irssi::active_win->print("");
  Irssi::active_win->print("/slackwiener add <name> - adds a bot to the list");
  Irssi::active_win->print("/slackwiener del <name> - remove a bot from the list");
  Irssi::active_win->print("/slackwiener list       - list bots");
  Irssi::active_win->print("/slackwiener help       - this help");
}

# Handle public messages
sub public_msg {
  my($server, $msg, $nick, $address, $target) = @_;

  if(exists($bot_nicks{$nick})) {
    substitute_nick($server, $msg, $nick, $address, $target);
    return;
  }

}

# Perform nick substitution
sub substitute_nick {
  my($server, $msg, $nick, $address, $target) = @_;


  my $channel = $server->channel_find($target);
  return if(!$channel);

  my $nick_rec = $channel->nick_find($nick);
  return if(!$nick_rec);

  # parse slack username from bot input
  my $newnick;
  my $message = 0;
  my $action;

  if($msg =~ /<([^>]*)>\s([^\n]*)/) {
    $newnick = $1 . "_slack";
    $message = $2;
    if($message =~ /File uploaded/) {
      if($ignore_images) {
       Irssi::signal_stop();
       return;
      } else {
        if($message =~ /File uploaded [^\s]* \/ (.*)/) {
          my $image_url = $1;

          $message = " shared a file: " . $image_url; 
          $action = 1;
        }
      }
    }
    
  } elsif($msg =~ /Action\:\s([^\s]*)\s([^\n]*)/) {
    $newnick = $1 . "_slack";
    $message = $2; 
    $action = 1;
  } else {
    Irssi::signal_stop();
  } 

  # Bail out if the message doesn't start with a nickname or is empty
  if(!$newnick || !$message) {
    Irssi::signal_stop();
    return;  
  }

  # Slack messages written with the slack bot's channel privileges
  my $temp = Irssi::Irc::Channel::nick_insert($channel, $newnick, $nick_rec->{op}, 0, $nick_rec->{voice}, 0);
  
  if(!$action) {
    Irssi::signal_emit("message public", $server, $message, $newnick, $address, $target);
  } else {
    Irssi::signal_emit("message irc action", $server, $message, $newnick, $address, $target);
  }

  $channel->nick_remove($temp);

  # Prevent the signal from bubbling up
  Irssi::signal_stop();

}


# Slackwiener command "parser
sub command_slack {
  my ($cmd, $arg) = split(/\s/, shift);
  if($cmd eq "add" && $arg) {
    add_bot($arg);
  } elsif($cmd eq "del" && $arg) {
    del_bot($arg);
  } elsif($cmd eq "list") {
    list_bots();
  } elsif($cmd eq "help") {
    print_help();  
  }else {
    Irssi::active_win->print("Unknown slackwiener command $cmd. Try /slackwiener help for a list of commands.");
  }
}


Irssi::signal_add("message public", "public_msg");
Irssi::signal_add("setup changed", \&reload_settings);
Irssi::settings_add_str("slackwiener", "slack_bots", "");
Irssi::settings_add_int("slackwiener", "ignore_images", 0); # some guys want to have it this way *shrug*
Irssi::command_bind("slackwiener", \&command_slack, "Slackwiener");
reload_settings(); # reload on init

