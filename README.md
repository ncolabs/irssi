# NCOLabs/irssi

Some scripts for Irssi


## Slackwiener.pl (v0.6)


Utility script that turns this:

< slackbot> < jrambo> ayyyyy

into this:

< jrambo_slack> ayyyyy


#### Usage

Copy 'slackwiener.pl' into your irssi scripts directory and load it.

##### Commands

###### /slackwiener add <nick> 

Adds <nick> into the list of bots to translate.

###### /slackwiener del <nick>

Removes <nick> from the bots list.


###### /slackwiener list

Display the current list


###### /slackwiener help

Help.

##### Settings

[slackwiener]
slack_bots      - comma separated list of bot nicks 
ignore_images   - Ignore image and file links (messages starting with File uploaded), 0 or 1. 
 



